var pessoa = { 
  nome: 'Junior',
  idade: 26,
  profissao: 'Desenvolvedor',
}

console.log(pessoa.nome)

var quadrado = { 
  lados: 4,
  area: function(lado) { 
    return lado * lado;
  },
  perimetro: function(lado) { 
    return this.lados * lado;
  },
  cinco()  { 
    return 5;
  },
  // Com a nova atualização não é mais necessario chamar a fucttion (perimetro: fucntion(lado)) agora basta (cinco(VALOR))
}

console.log(quadrado.area(5));
console.log(quadrado.perimetro(5));
// Console é um objeto  e quando coloco um ponto como >>> Console. após o ponto significa que estou passando um metodo como é o caso do log >>> console.log