// Atividade 1 - Declarar algumas variaveis

// Declarar uma variável com o seu nome
const nome = "Junior";

// Declarar uma variável com a sua idade
let idade = 25;

// Declarar uma variável com a sua comida
// favorita e não atribuir valor
var hambu;

// Atribuir valor a sua comida favorita
let comidaFavorita = "Hambu";

// Declarar 5 variáveis diferentes sem valores
var games, pais, lugar, biscoito, pastel;

console.log(nome, idade, hambu, comidaFavorita, pastel);


// Mutiplicação de valores

var precos = 25;
var TotalComprado = 10;
var totalPrecos = TotalComprado * precos;

console.log(totalPrecos)

// Uma unica variavel com valores difernetes

var sobreNome = 'Devas', 
    cidade = 'São Paulo';

console.log(sobreNome, cidade)

var nomeDoBrabo = 'The Jhujhu';

var idadeDoBrabo = 25;

var comidaDoBrabo;

var comidaDoBrabo = 'Pizza';

var semValor1 = 'Eu estou aqui',
    semValor2,
    semValor3,
    semValor4,
    semValor5;

console.log(nomeDoBrabo, idadeDoBrabo, comidaDoBrabo, semValor1, semValor2)