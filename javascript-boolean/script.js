var possuiGraduacao = true;

if(possuiGraduacao) {
  console.log('É verdadeiro')
} else { 
  console.log('É falso')
}

var developerJunior = true;
var developerPleno = false;
var developerSenior = true;

if(developerJunior) { 
  console.log('Sim, você é um desenvolvedor Junior');
} else if(developerPleno) { 
  console.log('Você ainda não é um desenvolvedor Pleno');
} else if(developerSenior) { 
  console.log('Um dia você será um desenvolvedor Senior');
}

var nome = 'Junior';

if(nome) { 
  console.log(nome);
}

// Quando temos uma váriavel sem dados ela
// é considerada false

var nome1 = '';

if(nome1) { 
  console.log(nome1);
} else { 
  console.log('Nome não existe')
}

if((5 - 5) && (5+5)) { 
  console.log('Verdadeiro');
} else { 
  console.log('Falso');
}


var condicional = (5 - 10) && (5 + 5);
if(condicional) { 
  console.log('Verdadeiro', condicional)
} else { 
  console.log('Falso')
}

// Me retorna o primeiro verdadeiro
var condicional2 = (5 - 5) || (20 + 5) || (10 - 2);
console.log('Verdadeiro', condicional2)

// Me retorna o ultimo verdadeiro
var condicional2 = (5 - 5) || (20 + 5) && (10 - 2);
console.log('Verdadeiro', condicional2)


// Switch
var corFavorita = 'Verde'

switch (corFavorita) { 
  case 'Azul':
    console.log('Olhe para o céu');
    break;
  case 'Amarelo':
    console.log('Olher para o sol');
    break;
  case 'Verde':
    console.log('Olhe para a floresta')
    break;  
  default:
    console.log('Feche os olhos')
}


// Verifique se a sua idade é maior do que a de algum parente
// Dependendo do resultado coloque no console 'É maior', 'É igual' ou 'É menor'
var idadeMae = 60;
var idadePai = 75;
var idadeFilho = 25;

if(idadeFilho > idadeMae) { 
  console.log('É maior');
} else if (idadeFilho === idadeMae) { 
  console.log('É igual');
} else { 
  console.log('É menor')
}


// Qual valor é retornado na seguinte expressão?
var expressao = (5 - 2) && (5 - ' ') && (5 - 2);
console.log(expressao)

// Verifique se as seguintes variáveis são Truthy ou Falsy
var nome = 'Andre';
var idade = 28;
var possuiDoutorado = false;
var empregoFuturo;
var dinheiroNaConta = 0;
console.log(!!nome, !!idade, !!possuiDoutorado, !!empregoFuturo, !!dinheiroNaConta);

// Compare o total de habitantes do Brasil com China (valor em milhões)
var brasil = 207;
var china = 1340;

if(brasil > china) { 
  console.log('Brasil é menor em habitantes')
} else { 
  console.log('Brasil é menor em habitantes')
}

// O que irá aparecer no console?
if(('Gato' === 'gato') && (5 > 2)) {
  console.log('Verdadeiro');
} else {
  console.log('Falso');
}

// O que irá aparecer no console?
if(('Gato' === 'gato') || (5 > 2)) {
  console.log('Gato' && 'Cão');
} else {
  console.log('Falso');
}
