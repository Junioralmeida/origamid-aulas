function areaQuadrado(lado) { 
  return lado * lado;
}

console.log(areaQuadrado(10))

function pi() { 
  return 3.14;
}

var total = 5 * pi(); //15.7
console.log(pi())
console.log(total)

function imc(peso, altura) { 
  var imc = peso / (altura * altura);
  return imc;
}

console.log(imc(100, 1.82))

function corFavorita(cor) { 
  if (cor === 'azul') { 
    return 'Eu gosto do céu';
  } else if(cor === 'verde') { 
    return 'Eu gosto da floresta';
  } else { 
    return 'Eu não gosto de nada';
  }
}

addEventListener('click', function() { console.log('Oi')});

function mostraConsole() { 
  console.log('Teste')
}

addEventListener('click', mostraConsole);


function imc2(peso, altura) {
  const imc = peso / (altura ** 2);
    console.log(imc);
  }
  
  imc2(80, 1.80);// retorna o imc
  console.log(imc2(80, 1.80));// retorna o imc e undefined


function terceiraIdade(idade) { 
  if(typeof idade !== 'number') { 
    return 'Por favor preencha um número'
  } else if(idade >= 60) {
    return true
  } else { 
    return false
  }
}

console.log(terceiraIdade(50))


function terceiraIdade2(idade) { 
  if(typeof idade !== 'number') { 
    return 'Por favor preencha um número'
  } else if(idade >= 60) {
    return 'Você é idoso'
  } else { 
    return 'Você ainda é jovem'
  }
}

console.log(terceiraIdade2(45))

function faltaVisitar(paisesVisitados) { 
  var totalPaises = 193;
  return `Falta visitar ${totalPaises - paisesVisitados} paises`;
}

console.log(faltaVisitar(30))

var profisao = 'Desenvovedor'

function dados() { 
  var nome = 'Junior';
  var idade = 26;
  function outrosDados() {
    var endereco = 'São Paulo';
    var idade = 25;
    return `${nome}, ${idade}, ${endereco}, ${profisao}`;
  }
  return outrosDados();
}

console.log(dados())


// Crie uma função para verificar se um valor é Truthy

function eVerdadeiro(dados) { 
  return !!dados;
}

// Crie uma função matemática que retorne o perímetro de um quadrado
// lembrando: perímetro é a soma dos quatro lados do quadrado

function perimetroQuadrado(lado) { 
  return lado * 4;
}

// Crie uma função que retorne o seu nome completo
// ela deve possuir os parâmetros: nome e sobrenome

function nomeCompleto(nome, sobreNome) { 
  return `${nome} ${sobreNome}`;
}

// Crie uma função que verifica se um número é par

function par(numero) { 
  var modulo = numero % 2;
  if(modulo === 0) { 
    return true;
  } else { 
    return false;
  }
}

// Crie uma função que retorne o tipo de
// dado do argumento passado nela (typeof)

function tipoDeDado(dado) { 
  return typeof dado;
}

// addEventListener é uma função nativa do JavaScript
// o primeiro parâmetro é o evento que ocorre e o segundo o Callback
// utilize essa função para mostrar no console o seu nome completo
// quando o evento 'click' ocorrer.

addEventListener ('click', function() { 
  console.log('Junior Almeida')
} )


// Corrija o erro abaixo
var totalPaises = 193;
function precisoVisitar(paisesVisitados) {
  return `Ainda faltam ${totalPaises - paisesVisitados} países para visitar`;
}
function jaVisitei(paisesVisitados) {
  return `Já visitei ${paisesVisitados} do total de ${totalPaises} países`;
}