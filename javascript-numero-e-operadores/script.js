// Tipo de dados

var nome = 'Andre';
console.log(typeof nome)

var idade = 25;
console.log(typeof idade)

var nomePlus = 'Rafael';
var sobreNome = 'Andre';
var nomeCompletoPlus = nomePlus + ' ' + sobreNome;

console.log(nomeCompletoPlus)

var gols = 1000;
var frase = 'Romário fez' + ' ' + gols + ' ' + 'gols';
console.log(frase)


var gols1 = 1000;
var frase1 = `Romário fez ${gols1 * 2} gols`
console.log(frase1)

// Declare uma variável contendo uma string

var declareUmastring = 'Olá eu sou uma string';
console.log(typeof declareUmastring)

// Declare uma variável contendo um número dentro de uma string

var declareUmastringComNumero = 'Olá eu sou uma string com número 25';
console.log(typeof declareUmastringComNumero)

// Declare uma variável com a sua idade

var idadeDaAna = 25;
console.log(idadeDaAna)

// Declare duas variáveis, uma com seu nome
// e outra com seu sobrenome e some as mesmas

var sobreNomeAna = 'Batistela'; 
var somaDeTudo = `${nomeAna} ${sobreNomeAna}`;


var nomeAna = 'Ana';
var sobreNomeAna2 = 'Batistela';
var somaTotalAna = nomeAna + ' ' + sobreNomeAna2;
console.log(somaTotalAna)
console.log(somaDeTudo)

// Coloque a seguinte frase em uma variável: It's time

var nomeVariavel = 'It\'s time';
var nomeVariavel = `Its time`;
var nomeVariavel = "Its time";
console.log(nomeVariavel)

// Verifique o tipo da variável que contém o seu nome
var nomeJuniorTeste = 'Junior'
console.log(typeof nomeJuniorTeste)


// Números e operadores
// Operadores aritmédicos binarios

var idade = 2e10;
var total = 10 + 5 + 10;
var modulo = 3872983892 % 3;

console.log(idade)

var testeNaN = 'Isso custa 100' / 2;
console.log(isNaN(testeNaN))

var soma1 = 10 + 10 + 20 + 30 * 4 / 2 + 10;
console.log(soma1)

// Posso priorizar a soma utilizando ( )
var soma2 = 10 + 10 + 20 + 30 * 4 / (2 + 10);
console.log(soma2)

// Operadores aritmédicos unários 

var x = 5;
console.log(x++)
console.log(x++)

var testeIdade = '25';
var testeSomaIdade = 5;
console.log(+testeIdade + testeSomaIdade)

// Qual o resultado da seguinte expressão?

var total = 10 + 5 * 2 / 2 + 20;
console.log(total)
// Resultado 35 >>> 5*2 = 10 / 2 = 5 + 20 = 25 + 10 = 35

// Crie duas expressões que retornem NaN

var nanExercicio1 = 'Olá eu vou me tornar um NaN' * 5;
console.log(nanExercicio1)

var nanExercicio2 = 'Olá eu vou me tornar um segundo Nan' / 20;
console.log(nanExercicio2)

// Somar a string '200' com o número 50 e retornar 250

var stringSoma = '200';
var stringSoma2 = 50;
console.log(+stringSoma + stringSoma2)

var stringSoma3 = +'200' + 50;
console.log(stringSoma3)

// Incremente o número 5 e retorne o seu valor incrementado

var incremento = 5;
console.log(incremento++)
console.log(incremento++)

var incremento1 = 5;
console.log(++incremento1)

// Como dividir o peso por 2?
var numero = '80';
var unidade = 'kg';
var peso = numero + unidade; // '80kg'
var pesoPorDois = peso / 2; // NaN (Not a Number)
console.log(+numero / 2 + unidade)

var numero1 = +'80' / 2;
var unidade1 = 'kg';
var peso1 = numero1 + unidade1; // '80kg'
console.log(peso1)